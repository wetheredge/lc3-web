use js_sys::Uint16Array;
use lc3::vm;
use lc3::{Ast, Program};
use std::convert::TryFrom;
use std::convert::TryInto;

pub mod utils;

use wasm_bindgen::prelude::*;

// When the `wee_alloc` feature is enabled, use `wee_alloc` as the global
// allocator.
#[cfg(feature = "wee_alloc")]
#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

static mut VM: lc3::VM = lc3::VM::new();

#[wasm_bindgen(js_name = "LC3")]
pub struct JsVM;

#[allow(clippy::unused_unit)] // TODO: remove on next release of wasm-bindgen
#[wasm_bindgen(js_class = "LC3")]
impl JsVM {
    #[wasm_bindgen(constructor)]
    pub fn new() -> Self {
        Self
    }

    pub fn check(&self, src: &str) -> bool {
        match Ast::parse(src) {
            Ok(ast) => Program::assemble(ast).is_ok(),
            Err(_) => false,
        }
    }

    pub fn load(&self, src: &str) {
        let program = Program::assemble(Ast::parse(src).unwrap()).unwrap();

        unsafe { VM.load(&program) };
    }

    pub fn run(&self) {
        unsafe { VM.run() }.unwrap();
    }

    #[wasm_bindgen(getter)]
    pub fn registers(&self) -> Uint16Array {
        let registers = unsafe { VM.registers() };
        let array = Uint16Array::new_with_length(8);

        for (i, int) in registers.into_iter().enumerate() {
            array.set_index(i as u32, u16::try_from(int).unwrap());
        }

        array
    }

    #[wasm_bindgen(getter)]
    pub fn memory(&self) -> Uint16Array {
        let memory = unsafe { VM.memory() };
        let array = Uint16Array::new_with_length(u16::MAX as u32);

        for index in 0..u16::MAX {
            let address = vm::MemoryIndex::new(index);
            array.set_index(index.into(), memory[address].try_into().unwrap());
        }

        array
    }

    #[wasm_bindgen(getter, js_name = "programCounter")]
    pub fn program_counter(&self) -> u16 {
        let address = *unsafe { VM.program_counter() };
        address.into()
    }

    #[wasm_bindgen(setter, js_name = "programCounter")]
    pub fn set_program_counter(&self, address: u16) {
        let address = vm::MemoryIndex::new(address);
        unsafe { VM.set_program_counter(address) };
    }

    #[wasm_bindgen(js_name = "getValueAt")]
    pub fn get_value_at(&self, address: u16) -> u16 {
        let address = vm::MemoryIndex::new(address);
        let memory = unsafe { VM.memory() };
        let value = memory[address];
        value.try_into().unwrap()
    }

    #[wasm_bindgen(js_name = "getMemorySlice")]
    pub fn get_memory_slice(&self, start: u16, end: u16) -> Uint16Array {
        let memory = unsafe { VM.memory() };
        let array = Uint16Array::new_with_length((end - start) as u32);

        for address in start..end {
            let value = memory[vm::MemoryIndex::new(address)];
            array.set_index(address as u32, value.into());
        }

        array
    }
}

impl Default for JsVM {
    fn default() -> Self {
        Self::new()
    }
}
