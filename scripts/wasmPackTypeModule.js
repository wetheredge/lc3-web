import editJsonFile from 'edit-json-file';
import path from 'path';

const file = editJsonFile(
	path.join(process.cwd(), 'lc3-wasm/pkg/package.json')
);
file.set('type', 'module');
file.save();
