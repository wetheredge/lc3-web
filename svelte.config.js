import adapter from '@sveltejs/adapter-static';
import wasmPack from 'vite-plugin-wasm-pack';
import preprocess from 'svelte-preprocess';

/** @type {import('@sveltejs/kit').Config} */
const config = {
	kit: {
		adapter: adapter({
			pages: 'public',
			precompress: true,
		}),

		vite: {
			optimizeDeps: {
				exclude: ['fs', 'os', 'path', 'url', 'lc3'],
			},
			plugins: [wasmPack(['./lc3-wasm'])],
		},
	},
	preprocess: preprocess(),
};

export default config;
