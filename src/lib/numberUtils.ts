export interface VmNumber {
	unsigned: number;
	signed: number;
	negative: bool;
}

export interface MemoryValue extends VmNumber {
	address: number;
}

export function unsigned(num: VmNumber): number {
	return num.unsigned;
}

export function formatHex(num: number): string {
	return `x${num.toString(16).toUpperCase().padStart(4, '0')}`;
}

export function formatUnsignedDec(num: VmNumber): string {
	return num.unsigned.toString(10);
}

export function formatSignedDec(num: VmNumber): string {
	return `${num.negative ? '-' : '+'}${num.signed.toString(10)}`;
}
